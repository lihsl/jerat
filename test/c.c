#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <gdbm-ndbm.h>
int main() {
	DBM * dbm = dbm_open("foo", O_RDWR | O_CREAT, 0666);
	if(dbm == NULL) {
		printf("dbm_open error");
		return 1;
	}
	datum dakey, davalue;
	int key = 1,value = 2;
	/*
	dakey.dptr = (void*)&key;
	dakey.dsize = sizeof key;
	davalue.dptr = (void*)&value;
	davalue.dsize = sizeof value;

	if(dbm_store(dbm, dakey, davalue, DBM_REPLACE) != 0) {
		puts("dbm_storage error");
	}
	*/
	for(dakey = dbm_firstkey(dbm); dakey.dptr; dakey = dbm_nextkey(dbm)) {
		davalue = dbm_fetch(dbm, dakey);
		if(davalue.dptr) {
			key = *(int*)dakey.dptr;
			value = *(int*)davalue.dptr;
			printf("%d %d\n", key, value);
		}
	}
	return 0;


}
