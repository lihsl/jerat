#ifndef SHELL_H
#define SHELL_H
#include "core.h"
int shell_init();
int shell_execute(struct context* ctx, char* cmd);
int shell_break(struct context* ctx);
void shell_uninit(struct context* ctx);
#endif
