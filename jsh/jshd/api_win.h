#ifndef API_WIN_H
#define API_WIN_H

#include <windows.h>

/* kernel32 */

typedef HMODULE WINAPI (*LoadLibraryA_f)(
  LPCSTR lpFileName
);

typedef FARPROC WINAPI (*GetProcAddress_f)(
  HMODULE hModule,
  LPCSTR  lpProcName
);

BOOL (*FreeLibrary_f)(
  HMODULE hLibModule
);

BOOL (*CloseHandle_f)(
  HANDLE hObject
);

typedef HANDLE (*GetProcessHeap_f)();

typedef LPVOID (*HeapAlloc_f)(
  HANDLE hHeap,
  DWORD  dwFlags,
  SIZE_T dwBytes
);

BOOL (*HeapFree_f)(
  HANDLE                 hHeap,
  DWORD                  dwFlags,
  LPVOID lpMem
);

LPVOID (*HeapReAlloc_r)(
  HANDLE                 hHeap,
  DWORD                  dwFlags,
  LPVOID lpMem,
  SIZE_T                 dwBytes
);

BOOL (*CreateProcessA_f)(
  LPCSTR                lpApplicationName,
  LPSTR                 lpCommandLine,
  LPSECURITY_ATTRIBUTES lpProcessAttributes,
  LPSECURITY_ATTRIBUTES lpThreadAttributes,
  BOOL                  bInheritHandles,
  DWORD                 dwCreationFlags,
  LPVOID                lpEnvironment,
  LPCSTR                lpCurrentDirectory,
  LPSTARTUPINFOA        lpStartupInfo,
  LPPROCESS_INFORMATION lpProcessInformation
);

BOOL (*CreatePipe_f)(
  PHANDLE               hReadPipe,
  PHANDLE               hWritePipe,
  LPSECURITY_ATTRIBUTES lpPipeAttributes,
  DWORD                 nSize
);

HANDLE WINAPI (*GetStdHandle_f)(
  _In_ DWORD nStdHandle
);



#endif
