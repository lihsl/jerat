global _RelocateAsset
global _s_Ws2_32_dll
global _s_WSAStartup
global _s_WSASocketA
global _s_WSAConnect
global _s_WSARecv

section .text
_RelocateAsset:
	enter 0, 0
	call pusheip
strtab:
_s_Ws2_32_dll: db "Ws2_32.dll", 0
_s_WSAStartup: db "WSAStartup", 0
_s_WSASocketA: db "WSASocketA", 0
_s_WSAConnect: db "WSAConnect", 0
_s_WSARecv   : db "WSARecv", 0

pusheip:
	mov eax, [esp]
	mov ecx, [ebp + 8]
	sub ecx, strtab
	lea eax, [eax + ecx]
	add esp, 4
	leave
	ret




