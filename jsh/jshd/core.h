#ifndef CORE_H
#define CORE_H
#include "def.h"
/*typedef unsigned long size_t;*/
int core_init(context_t *ctx);

context_t *getctx();
int geterror();


dl_t dlopen(const char* filename);
void *dlsym(dl_t handle, char *symbol);
void dlclose(dl_t handle);

void *malloc(size_t size);
void *remalloc(void *ptr, size_t size);
void free(void* ptr);

int exec(const char *pathname, const char *arg);

file_t *fopen(const char *filename);
file_t *popen(const char *cmdline);
file_t *topen(const char *address);
void fclose(file_t *f);
int fprintf(file_t *f, char *fmt, ...);
int fputs(file_t *f, char *s);
int fgetc(file_t *f);
int fputc(file_t *f, char c);
size_t fread(file_t *f, void *ptr, size_t size);
int fwrite(file_t *f, void *ptr, size_t size);
int ftrunc(file_t *f);
int fseek(file_t *f, size_t offset);

#endif
