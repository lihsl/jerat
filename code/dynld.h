#ifndef DYNLD_H
#define DYNLD_H
#include <windef.h>
typedef struct {
	CHAR data[12];
} STRING12;


LPVOID FindInKernel32(
		STRING12 sFunction
		);

extern LPVOID Find_LoadLibraryA();

extern LPVOID Find_GetProcAddress();
#endif /* DYNLD_H */
