CODE = $(ROOT)/code/code.a

$(BIN): $(EXE)
	objcopy -O binary -j .text $^ $@
	@stat -c "Payload total %s bytes." $(BIN)
disas: $(BIN)
	ndisasm -b32 $^

test: test.c
	$(CC) $(CFLAGS) -fno-stack-protector  -o $@.exe $^
test.c: $(BIN)
	@xxd -i $^ > $@
	@echo "int main(){return ((int(*)())$(subst .,_,$(BIN)))();}" >> $@

$(CODE):
	cd $(ROOT)/code; make
