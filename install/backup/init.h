#ifndef COMMON_H
#define COMMON_H
#define _WIN32_WINNT 0x0501
#include <windows.h>
#include <string.h>
#define SERVICE_NAME 		"jerat"
//#define SERVICE_BINFILE		"jerat.exe"

typedef DWORD (WINAPI  *PFN_WTSGetActiveConsoleSessionId)(VOID);
typedef BOOL  (WINAPI  *PFN_WTSQueryUserToken)(DWORD,PHANDLE);

extern PFN_WTSGetActiveConsoleSessionId	WTSGetActiveConsoleSessionId;
extern PFN_WTSQueryUserToken			WTSQueryUserToken;

extern BOOL IsSystemWOW64();


extern BOOL InitEnv();
//extern VOID RunInSession(TCHAR *szModuleFile,DWORD dwSessionId);

#endif // COMMON_H
