#include "init.h"
#include <userenv.h>
#include <stdio.h>
//#include <wtsapi32.h>
PFN_WTSGetActiveConsoleSessionId WTSGetActiveConsoleSessionId;
PFN_WTSQueryUserToken WTSQueryUserToken;
//CHAR szStartUpFileName[MAX_PATH]={0};

int InitEnv()
{
	HMODULE hModule;
	/*
    GetWindowsDirectoryA(szStartUpFileName,MAX_PATH);
    lstrcat(szStartUpFileName,IsSystemWOW64() ?
               ("\\SysWOW64\\Ixil\\" STARTUP_FILE):
               ("\\System32\\Ixil\\" STARTUP_FILE);
			   */
	hModule = LoadLibraryA("Kernel32.dll");
	if(hModule == NULL)
		return 1;
    WTSGetActiveConsoleSessionId = 
		(PFN_WTSGetActiveConsoleSessionId) GetProcAddress(hModule, "WTSGetActiveConsoleSessionId");
    if (WTSGetActiveConsoleSessionId == NULL)
        return 1;

	hModule = LoadLibraryA("wtsapi32.dll");	
	if(hModule == NULL)
		return 1;

    WTSQueryUserToken =
		(PFN_WTSQueryUserToken) GetProcAddress(hModule, "WTSQueryUserToken");
    if(WTSQueryUserToken == NULL)
		return 1;

    return 0;
}

BOOL IsSystemWOW64()
{
    typedef BOOL (WINAPI *LPFN_ISWOW64PROCESS) (HANDLE, PBOOL);
    LPFN_ISWOW64PROCESS fnIsWow64Process;
    BOOL bIsWow64 = FALSE;
    fnIsWow64Process = (LPFN_ISWOW64PROCESS)
		GetProcAddress(GetModuleHandleA("kernel32"), "IsWow64Process");

    if (NULL != fnIsWow64Process)
        fnIsWow64Process(GetCurrentProcess(),&bIsWow64);
    return bIsWow64;
}


